(scroll-bar-mode -1)
;; use-package installation:
(require 'package)
(setq package-enable-at-startup nil)
(add-to-list 'package-archives'("melpa" . "https://melpa.org/packages/"))
(add-to-list 'package-archives'("melpa-stable" . "https://stable.melpa.org/packages/") t)
(package-initialize)
;; Bootstrap `use-package'
(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))

(set-default-font "Hack 12" nil t)

(use-package doom-modeline
  :ensure t
  :hook (after-init . doom-modeline-mode))

(use-package treemacs
  :ensure t
  :config
  (setq treemacs-width 23)
  :bind
  (("C-c m" . treemacs)))

(use-package doom-themes
  :ensure t
  :config
  (setq prelude-theme 'doom-dracula))

(use-package ace-window
  :ensure t
  :bind
  ("C-c w" . 'ace-window))
